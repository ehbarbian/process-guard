#ifndef PROCESS_KILL_H
#define PROCESS_KILL_H

#include <windows.h>

#define TA_FAILED 0
#define TA_SUCCESS_CLEAN 1
#define TA_SUCCESS_KILL 2

DWORD WINAPI TerminateApp( DWORD dwPID, DWORD dwTimeout );
                        
#endif
