#ifndef POINTERVIEW_H
#define POINTERVIEW_H
#include <windows.h>

class PointerView
{
	public:
		PointerView(int);
		~PointerView();
		bool verify();
		void reset();
		const int& getCounter();
	private:
		POINT p;
		int counter;
		int times;
		long x;
		long y;
	
};

#endif
