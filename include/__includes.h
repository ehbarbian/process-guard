#ifndef __INCLUDES_H
#define __INCLUDES_H

#include <ctime>
#include <iostream>
#include <fstream>

#include <Lmcons.h>

#include <PointerView.h>
#include <WindowView.h>
#include <ProcessKill.h>
#include <INIReader.h>

#define U_LEN (UNLEN + 1)
#define MAX_NAME 256

using namespace std;

void to_lower(char* p){
	while(*p){
		*p = tolower(*p);
		++p;
	}
}

void log(const char* text){
	ofstream file;
	file.open("log.txt", ios::out | ios::app);
	
	if(file.is_open()){
		TCHAR user[U_LEN];
		DWORD size = U_LEN;
		GetUserName(user, &size);
		
		time_t t = time(0);
		struct tm* now = localtime(&t);
		file << "[ "	<< (now->tm_mday)
			 << "-"		<< (now->tm_mon + 1)
		 	 << "-"		<< (now->tm_year + 1900)
			 << "  "	<< (now->tm_hour)
			 << ":"		<< (now->tm_min)
			 << ":"		<< (now->tm_sec)
			 << "] { "	<< user
			 << "} "	<< text << endl;
	}else{
		cout << "N�o foi possivel salvar o log: " << text << endl;
	}
}

#endif
