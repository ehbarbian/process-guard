#ifndef WINDOWVIEW_H
#define WINDOWVIEW_H
#include <windows.h>

class WindowView
{
	public:
		WindowView(int);
		~WindowView();
		bool verify(DWORD pid);
		void reset();
		const int& getCounter();
	private:
		int counter;
		int times;
};

#endif
