#ifndef PROCESSKILL_H
#define PROCESSKILL_H

#include <windows.h>

#define TA_FAILED 0
#define TA_SUCCESS_CLEAN 1
#define TA_SUCCESS_KILL 2

class ProcessKill
{
	public:
		ProcessKill();
		DWORD WINAPI TerminateApp( DWORD dwPID, DWORD dwTimeout );
};

#endif
