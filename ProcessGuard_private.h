/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef PROCESSGUARD_PRIVATE_H
#define PROCESSGUARD_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"1.0.0.0"
#define VER_MAJOR	1
#define VER_MINOR	0
#define VER_RELEASE	0
#define VER_BUILD	0
#define COMPANY_NAME	"Eduardo Henrique Barbian"
#define FILE_VERSION	"1.0.0.0"
#define FILE_DESCRIPTION	"ProcessGuard - Process View and Terminate Program"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	"Copyright 2016 EHB - Do Not Distribute"
#define LEGAL_TRADEMARKS	"ProcessGuard & EHB are Eduardo Henrique Barbian Trademarks"
#define ORIGINAL_FILENAME	"ProcessGuard"
#define PRODUCT_NAME	"ProcessGuard"
#define PRODUCT_VERSION	"1.0.0.0"

#endif /*PROCESSGUARD_PRIVATE_H*/
