#include "PointerView.h"

PointerView::PointerView(int t) : times(t){
	reset();
}

PointerView::~PointerView(){}

const int& PointerView::getCounter(){
	return counter;
}

bool PointerView::verify(){
	GetCursorPos(&p);
	
	if(p.x == x && p.y == y){
		counter++;
		
		if(counter >= times){
			return true;
		}
		
	}else{
		x = p.x;
		y = p.y;
		counter = 0;
	}
	
	return false;
}

void PointerView::reset(){
	x = 0;
	y = 0;
	counter = 0;
}
