#include "WindowView.h"

WindowView::WindowView(int t) : times(t){
	counter = 0;
}

WindowView::~WindowView(){
	
}

const int& WindowView::getCounter(){
	return counter;
}

bool WindowView::verify(DWORD pid){
	DWORD cPid;
	GetWindowThreadProcessId(GetForegroundWindow(), &cPid);
	
	if( pid != cPid ){
		counter++;
		
		if(counter >= times){
			return true;
		}
		
	} else {
		counter = 0;
	}
	
	return false;
}

void WindowView::reset(){
	counter = 0;
}

