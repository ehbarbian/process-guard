#include <__windows.h>
#include <__includes.h>
/* #define __DEBUG__ */

void startGuard(char* pName, char* uName, int time, int sleepTime);
DWORD getPidByName(const char*, const char*);
BOOL GetProcessOwner (HANDLE, char*);

void showHelp(){
	cout << "processguard.exe <nome_processo> <tempo_segundos>";
	exit(0);
}

int parseINI(char* pName, int& timek, int& sleepk){
	INIReader reader("config.ini");

    if (reader.ParseError() < 0) {
        log("Nao foi poss�vel abrir o arquivo de configuracao!");
        return 0;
    }
    
    const string& s = reader.Get("geral","processo","<nenhum>");
	strcpy(pName, s.c_str());
	timek = reader.GetInteger("geral","tempo",60*10);
	sleepk = reader.GetInteger("geral","sleep_ativo",1000);
    return 1;
}

int main(int argc, char** argv){
	char pName[256];
	int time, sleepTime = 1000;
	
	if( argc == 2 && strcmp(argv[1],"--help") == 0 )
		showHelp();

	short pIni = parseINI(pName,time,sleepTime);
	//cout << pName << " | " << time << " | " << sleepTime << endl;
	
	if ( pIni == 0 && argc == 3){
		strcpy(pName, argv[1]);
		time = atoi(argv[2]);
	
	} else if ( pIni == 0 && argc != 3){
		showHelp();
	}
	
	to_lower(pName);
	char* uName = getenv("USERNAME");
	startGuard(pName, uName, time, sleepTime);
	
	return 0;
}

void startGuard(char* pName, char* uName, int time, int sleepTime){
	PointerView pv(time);
	WindowView wv(time);
	DWORD pid;
	
	
	while(1){
		pid = getPidByName(pName, uName);
		
	    if(pid != 0){
	    	#ifdef __DEBUG__
	    	cout << "PID: " << pid << endl;
	    	#endif
	    	
	    	if ( pv.verify() || wv.verify(pid) ){
				#ifdef __DEBUG__
	    		cout << "Derrubar: " << TerminateApp(pid, 2000) << endl;
				#else
				TerminateApp(pid, 2000);
				#endif
	    		
				pv.reset();
				wv.reset();
			#ifdef __DEBUG__
			} else {
				cout << "Contando: <PV | " << pv.getCounter() << "> <WV | " << wv.getCounter() << ">" << endl;
			#endif
			}
						
			Sleep(sleepTime);
			
		}else{
			#ifdef __DEBUG__
			cout << "Nenhum processo!" << endl;
			#endif
			pv.reset();
			wv.reset();
			
			Sleep(10000);
		}
		
		//Sleep(2000);
	}
}

DWORD getPidByName(const char* processName, const char* userName){
	// Lista todos os Processos
	DWORD aProcesses[1024], cbNeeded, cProcesses;
	unsigned int i;
	
	if (!EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded)) {
		log("N�o foi poss�vel listar os processos.");
		return 0;
	}
	    
	// Calcula quantos PID foram retornados
	cProcesses = cbNeeded / sizeof(DWORD);
	
	for ( i = 0; i < cProcesses; i++ ) {
		if( aProcesses[i] != 0 ){
			char szProcessName[MAX_PATH];
			HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, aProcesses[i] );
			
			if (NULL != hProcess ){
        		HMODULE hMod;
        		DWORD cbNeeded;
        		
        		if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod), &cbNeeded) ) {
        			char userbuff[MAX_NAME];
        			
            		GetModuleBaseName( hProcess, hMod, szProcessName, sizeof(szProcessName)/sizeof(TCHAR) );
            		GetProcessOwner( hProcess , userbuff);
            		
					to_lower(szProcessName);
            		if(strcmp(processName, szProcessName) == 0 && strcmp(userName, userbuff) == 0){
            			CloseHandle( hProcess );
            			return aProcesses[i];
					}
        		}
    		}
    		CloseHandle( hProcess );
		}
	}
	
	return 0;
}

BOOL GetProcessOwner (HANDLE hProcess, char* userbuff){
	HANDLE hToken = NULL;
	PTOKEN_USER ptu = NULL;
	DWORD dwLength = 0, dwSize = 256;
	BOOL bSuccess = FALSE;

    if( !OpenProcessToken(hProcess, TOKEN_QUERY, &hToken) ){
    	return FALSE;
    }
    
    if ( !GetTokenInformation(hToken, TokenUser, (LPVOID) ptu, 0, &dwLength)) {
    	
		if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
			goto Cleanup;

		ptu = (PTOKEN_USER) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwLength);

		if (ptu == NULL)
			goto Cleanup;
	}
	
	if (!GetTokenInformation(hToken, TokenUser, (LPVOID) ptu, dwLength, &dwLength)) {
		goto Cleanup;
	}
	
	SID_NAME_USE SidType;
	
	if( !LookupAccountSid( NULL , ptu->User.Sid, userbuff, &dwSize, NULL, &dwSize, &SidType)) {
		
		if( GetLastError() == ERROR_NONE_MAPPED )
			strcpy (userbuff, "NONE_MAPPED" );
			
	} else {
		bSuccess = TRUE;
	}

	Cleanup: 

	if (ptu != NULL)
		HeapFree(GetProcessHeap(), 0, (LPVOID)ptu);
	
    CloseHandle( hToken );
    
    return bSuccess;
}

