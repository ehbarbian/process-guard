# ProcessGuard

Idle process monitor based on Win32 API.

### Details
  - INI based configuration file
  - command-line configuration
  - find PID based on image name (i.e. app.exe)

### Version
  - 0.1

### License
  - See [LICENSE]
    
#### Contact
  - [Eduardo]

[Eduardo]: <mailto:ehbarbian@gmail.com>
[LICENSE]: https://gitlab.com/ehbarbian/process-guard/blob/master/LICENSE